#include <stdio.h> 
#include <string.h> 
#include <stdint.h> 
#include <stdlib.h> 
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#define MAX 50
#define HIST_SIZE 20

int main();           // main function to run the function
void displayPrompt(); // Display the prompt for user
void exitProgram();   // Will exit the program anytime it is called
void executeCom();    // Will allow the user to execute external commands from the put shell 
char *token_array[MAX]; //token array to hole tokenized input

// writefile function
void writeFile(char *history[HIST_SIZE],char *filename){
int a = 0;
FILE *filewriter; 
filename = strcat(getenv("HOME"),"/.hist_list"); //set's filoe name to the directory of the file by concatination
filewriter = fopen(filename,"w"); // open the file in order to wrtie the history to the file
 if(filewriter != NULL){   //checks if the file exsists
  while(history[a] != 0){  // loop through the history until it finds a 0
   fprintf(filewriter,"%s\n",history[a]); // print the history to the file
   a++;
   if(a == HIST_SIZE){ //if a, which is the count, is equal to the history size then stop
    printf("The full history has been saved\n");
    break;
   }
  }
 fclose(filewriter); //close the file for reading
 }else{
  printf("Cannot open file to store history\n");
  exit(1);
 }
}

// addHistory Function
void addHistory(char input[513],char *history[HIST_SIZE]){
 strtok(input,"\n");
 if(input[0] != '\n'){
  if(strncmp("exit",input,512) != 0){
   if(strncmp("!",input,1) != 0){
    if(strncmp("history",input,512)!= 0){
     if(history[19] != 0){ // checks if the history is full 
     int j = 0;
     while((HIST_SIZE-1) > j){ // if full loop through the array and move all elements down one 
      history[j] = history[j+1];
      j++;
   }
  }
  history[HIST_SIZE-1] = 0;
  int i = 0;
  while(history[i] != 0){ //loops round till it finds the first empty space
    i++;
  }
   history[i] = strdup(input); //once the empty space is found put the input into history
    }
   }
  }
 }
}
// displayPrompt Function
// Display a prompt for the user and be able to get input from the user
// Stop's when user types "exit" or when Ctrl-D is pressed
void displayPrompt(char *savedPath, char *history[HIST_SIZE], char *filename,char *alias[10][2]){
 char cwd[256];
 char input[513]; //input thats 512 characters long
 char unwanted[513]; //to store unwanted data
 const char delimiter[9] = "; |\n\t&<>"; // qutations for space
 char *token; // a token that will store the input and used to check input
 int found = 1;
 int j=0; 
 char copyInput[513];
 int exitCheck = 0;

 if(cwd != NULL){
    printf("%s>",getcwd(cwd,sizeof(cwd)));
 }else {
   printf("Please Prompt>");//print User prompt to consle
 }

 if((fgets(input,513,stdin)) == NULL){ // Ctrl-D makes fgets return a NULL so this checks for if Ctrl-D has been pressed
   printf("\n");
   writeFile(history,filename);
   exitProgram(savedPath);
   }else {
      for(int i = 0; i<512;i++){ // looks for the '\0' symbol in the input if it doesnt find it the output is too big
       if (input[i] == '\0'){
        found = 0;
   } 
 }

 strcpy(copyInput,input); //make a copy of the input to be stored into history

 if(found == 1){ //print an error if the input it too big 
  printf("Error char limit reached\n");
  fgets(input,513,stdin); //read unwanted data (i.e new line)
  fgets(input,513,stdin); //read unwanted data (i.e characters over the limit)
 }
 if (!strcmp(input,"exit\n")){ // checks to see if the input is "exit" and if so exit the program
       exitCheck = 1;
 } 

 addHistory(copyInput,history); // save the copied input into history 

 if(exitCheck == 1){ //is the user has typed in exit start the exit sequence
   writeFile(history,filename);
   exitProgram(savedPath);
 }
 strtok(input,"\n");
 token = strtok(input,delimiter);// token is set to the input including the space's

if (input[0] == '\n' && input[1] == '\0'){ // checks if the input is emtpy and if so display that no input was detected
       printf("No input detected\n");
       
 }else {
   while(token != NULL && found == 0) { // if the token is not null then print out the token then set it to null after it has printed
            token_array[j] = token;
            token = strtok(NULL,delimiter);
     	    printf( "'%s'\n", token_array[j]);
            j++;
    }
    executeCom(cwd,history,alias);
   }
  }
}

// Set path function
void setpath(){
 if(token_array[1] != NULL){
    if(setenv("PATH",token_array[1],1) == 0){
     printf("The new path is %s\n", token_array[1]);
     printf("Path changed\n");
   }else {
       printf("Invalid path\n");
         }
 }else if(token_array[1] == NULL){
  printf("Only need at least one parameter\n");
}
return;
}

// Get path function
void getpath(){
  if(token_array[2] != NULL){
   printf("The getpath command does not take parameters\n");
  } else{
   printf("The current path is %s\n",getenv("PATH"));
}
return;
}

// cd Function
void cd(char *cwd){
 if(token_array[1] == NULL){
  chdir(getenv("HOME"));
} else if (token_array[1] != NULL && token_array[2] == NULL){
    if(chdir(token_array[1]) == 0){
     getcwd(cwd,sizeof(cwd));
    }else {
      perror("Error:");
     }

}else if (token_array[2] != NULL){
  printf("Too many parameters for the cd command");
 }
}

// historyEx function
void historyEx(char *cwd, char *history[HIST_SIZE]){
if(strcmp("history",token_array[0]) == 0){// if the input was history then just proceed to print out the history
 if(history[0] == 0){ // checks to see if history is empty
  printf("History is empty\n");
 }else if(history[0] != 0){ // if its not empty then print out the history
  for(int i=0; i < HIST_SIZE;i++){
   if(history[i] != 0){
    printf("%d. %s\n",i+1,history[i]);
    }
   } 
  }
 }else if (strcmp("!",token_array[0]) == 0){ // else if the input started with a ! then try to execute the command
  if(token_array[1] == NULL){  // checks to see if the a number was provided
     printf("No number detected in the brackets\n");
 }else{
  int pos = 0;
  pos = atoi(token_array[1]);   // converts the provided number from char to and int
  pos--;
  if(pos >= 0 && pos <= 19){
   if(history[pos] != 0){   // checks if that position in memory is not empty
    char command[513];
    char *token;
    int j = 0;
    strcpy(command,history[pos]);  // copies the command from the history position into the command
    token = strtok(command,"; |\n\t&<>");  //set's up the token 
    while(token != NULL) { // if the token is not null then print out the token then set it to null after it has printed
       token_array[j] = token;
       token = strtok(NULL,"; |\n\t&<>");
       j++;
    }
    if(j <= 1){
      token_array[1] = NULL;
    }
    printf("Will execute command %s from history\n",history[pos]);
    executeCom(cwd,history); // execute the command that has been placed in the token_array accordingly
 }else{ // if the memeory location is empty then through a error measage to the screen
     printf("That location in history is empty\n");
   }
 }else if(pos < 0){   // checks that the number provided is positive
   printf("You have to provide a positive number\n");
 }else if(pos >= 19){
    printf("Out of bounds, Has to be a number between 1 to 20\n");
   }
  }
 }
} 

void addAlias(char *alias[10][2]){
 printf("ok\n");
}

void unAlias(char *alias[10][2]){
 printf("ok\n");
}

// Execute command function 
void executeCom(char *cwd, char *history[HIST_SIZE],char *alias[10][2]){
 if(strcmp("setpath",token_array[0]) == 0){
   setpath();
} else if (strcmp("getpath",token_array[0]) == 0){
   getpath();
}else if (strcmp("cd",token_array[0]) == 0){
  cd(cwd);
}else if (strncmp("!", token_array[0],1) == 0 || strncmp("!!",token_array[0],1) == 0){
  historyEx(cwd,history);
}else if(strcmp("history",token_array[0]) == 0){
 historyEx(cwd,history);
}else if(strcmp("alias",token_array[0]) == 0){
 addAlias(alias);
}else if(strcmp("unalias",token_array[0]) == 0){
 unAlias(alias);
}else{

 int status;
 pid_t pid;
 pid = fork();

 if(pid < 0){
    // If pid returns a number < 0 (i.e. -1) then and then the user is alerted
    printf("An error has occured proccess failed\n");
    exit(EXIT_FAILURE);
 }  
  else if(pid == 0) {
    // If the pid = 0 we are in the child process and we can run the inputted system command
   if(execvp(token_array[0],token_array) == -1){
    printf("Program not found \n");
   }
   exit(2); //kill the fork()
}else{
   // When pid > 0 then we are in the parent process and it will wait for the command to be ran 
   while(wait(&status) != pid);
 }
}
}

//exitProgram Function
void exitProgram(char *savedPath){
printf("The path that is restored is %s\n", savedPath);
printf("Exiting program....\n");
setenv("PATH",savedPath,1);
exit(0);
}

// main function
int main(){
   char *history[HIST_SIZE] = {0}; //history array
   char *savedPath; 	
   char *filename;
   FILE *file;
   char cwd[256];
   char *alias[10][2] = {{0}};
   savedPath = getenv("PATH");
   chdir(getenv("HOME"));

   //read history from file
   filename = strcat(getcwd(cwd,sizeof(cwd)),"/.hist_list"); //filename is set to the path and the file name using concatination
   file = fopen(filename,"r");  // open the file to read in the history from the file
   if(!file == 0){   //checks if the file exsists
    char lineIn[513];
    int a  = 0;
    while(fgets(lineIn, sizeof lineIn,file) != NULL && (a<20)){  //loops through each line of file and stores it in lineIn
       history[a] = strdup(strtok(lineIn,"\n"));  //set the history location to the a line thats been read from the file
       a++;
     }
    fclose (file);  //close the file
    }else{
  printf("There is no history file detected, so one will be created on exit\n");
}
   printf("--------------------\n");
   printf("ACE4: A Simple Shell\n");
   printf("--------------------\n");

 while(1){  // continously call the the user prompt for a constant stream of input from the user
    for(int i = 0; i < MAX; i++){ //as the token_array is global initialise it after each inputed command
      token_array[i] = NULL;
   }
    displayPrompt(savedPath,history,filename,alias); //call the display prompt funciton to get input form user
 }
return(0);
}

