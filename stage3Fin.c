#include <stdio.h> 
#include <string.h> 
#include <stdint.h> 
#include <stdlib.h> 
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#define MAX 50

int main();           // main function to run the function
void displayPrompt(); // Display the prompt for user
void exitProgram();   // Will exit the program anytime it is called
void executeCom();    // Will allow the user to execute external commands from the put shell 
char *token_array[MAX];



// Display a prompt for the user and be able to get input from the user
// Stop's when user types "exit" or when Ctrl-D is pressed
void displayPrompt(char *cwd, char *savedPath){

 char input[513]; //input thats 512 characters long
 char unwanted[513]; //to store unwanted data
 const char delimiter[9] = "; |\n\t&<>"; // qutations for space
 char *token; // a token that will store the input and used to check input
 int found = 1;
 int j=0; 


 if(cwd != NULL){
    printf("%s>",cwd);
 }else {
   printf("Please Prompt>");//print User prompt to consle
 }

 if((fgets(input,513,stdin)) == NULL){ // Ctrl-D makes fgets return a NULL so this checks for if Ctrl-D has been pressed
     printf("\n");
     exitProgram(savedPath);   
   }else {
      for(int i = 0; i<512;i++){ // looks for the '\0' symbol in the input if it doesnt find it the output is too big
       if (input[i] == '\0'){
        found = 0;
   } 
 }

 if(found == 1){ // print an error if the input it too big 
  printf("Error char limit reached\n");
  fgets(input,513,stdin); //read unwanted data (i.e new line)
  fgets(input,513,stdin); //read unwanted data (i.e characters over the limit)
 }

 if (!strcmp(input,"exit\n")){ // checks to see if the input is "exit" and if so exit the program
       exitProgram(savedPath);
 } 

 strtok(input,"\n");
 token = strtok(input,delimiter);// token is set to the input including the space's

if (input[0] == '\n' && input[1] == '\0'){ // checks if the input is emtpy and if so display that no input was detected
       printf("No input detected\n");
       
 }else {
   while(token != NULL && found == 0) { // if the token is not null then print out the token then set it to null after it has printed
            token_array[j] = token;
            token = strtok(NULL,delimiter);
     	    printf( "'%s'\n", token_array[j]);
            j++;
    }
    executeCom();
   }
  }
}

// Set path function
void setpath(){
 if(token_array[1] != NULL){
    if(setenv("PATH",token_array[1],1) == 0){
     printf("The new path is %s\n", token_array[1]);
     printf("Path changed\n");
   }else {
       printf("Invalid path\n");
         }
 }else if(token_array[1] == NULL){
  printf("Only need at least one parameter\n");
}
return;
}

// Get path function
void getpath(){
  if(token_array[2] != NULL){
   printf("The getpath command does not take parameters\n");
  } else{
   printf("The current path is %s\n",getenv("PATH"));
}
return;
}

// Execute command function 
void executeCom(){
 if(strcmp("setpath",token_array[0]) == 0){
   setpath();
} else if (strcmp("getpath",token_array[0]) == 0){
   getpath();
}else{

 int status;
 pid_t pid;
 pid = fork();

 if(pid < 0){
    // If pid returns a number < 0 (i.e. -1) then and then the user is alerted
    printf("An error has occured proccess failed\n");
    exit(EXIT_FAILURE);
 }  
  else if(pid == 0) {
    // If the pid = 0 we are in the child process and we can run the inputted system command
   if(execvp(token_array[0],token_array) == -1){
    printf("Program not found \n");
   }
   exit(2); //kill the fork()
}else{
   // When pid > 0 then we are in the parent process and it will wait for the command to be ran 
   while(wait(&status) != pid);
 }
}
}


void exitProgram(char *savedPath){
printf("Exiting program....\n");
printf("The path that is restored is %s\n", savedPath);
setenv("PATH",savedPath,1);
exit(0);
}

// main function
int main(){
   char *cwd;
   char *savedPath; 	
   savedPath = getenv("PATH");
   cwd = getenv("HOME");
   chdir(getenv("HOME"));
 while(1){  // continously call the the user prompt for a constant stream of input from the user
    for(int i = 0; i < MAX; i++){
      token_array[i] = NULL;
   }
    displayPrompt(cwd, savedPath);
 }
return(0);
}

